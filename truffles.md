# Brazilian coconut truffles


## Ingredients: 
- 1 can of sweetened condensed milk 
- 15 grams unsalted butter 
- 1/2 cup of coconut milk
- coconut flakes


Instructions: 
1. In a medium saucepan over medium heat, combine the coconut milk and the sweetened condensed milk, mix well with a whisk and add the unsalted butter. 
2. Be sure to keep stirring and cooking the mixture over medium heat for 15 to 18 minutes, until thickened. Important: do NOT let the brigadeiro burn. 
3. Turn off the stove and if you wish,  Mix well for 2 minutes until it's completely melted.
4. Carefully, transfer or spread the brigadeiro mixture on a plate, cover with plastic wrap and let rest until cool enough to handle. 
5. Put a little of the unsalted butter on your hands and hand roll the mixture into small little smooth balls.
6. Roll the finished balls in a topping of your choice. coconut flakes
7. Enjoy with your family and friends!





  truffles.md  0 → 100644
 

# Brazilian chocolate truffles


## Ingredients: 

- 1 can of sweetened condensed milk 
- 15 grams unsalted butter 
- 30 grams of pure cocoa powder 
- 1 vanilla pod

## Additional ingredient if you want to add a richer flavour:

- 50 grams of good quality chocolate (semi-dark)

## Instructions: 

1. In a medium saucepan over medium heat,melt some butter.
2. Combine the pure cocoa powder and the sweetened condensed milk, mix well with a whisk 
3. Be sure to keep stirring and cooking the mixture over medium heat for 15 to 18 minutes, until thickened. **Important: do NOT let the brigadeiro burn.** 
3. Turn off the heat and if you wish, add the pieces of pure chocolate. Mix well for 2 minutes until it's completely melted. Add vanilla pod.
4. Carefully, transfer or spread the brigadeiro mixture on a plate, cover with plastic wrap and let rest until cool enough to handle. 
5. Put a little of the unsalted butter on your hands and hand roll the mixture into small little smooth balls.
6. Roll the finished balls in a topping of your choice. My favorites are chocolate sprinkles or shaved chocolate bits
7. Enjoy with your family and friends!
